<?php

//pokretanje sessiona i dohvat svih fileova na serveru
session_start();

$targetDir = "uploads/";

$files = array_diff(scandir($targetDir), array('.', '..'));

if (isset($_SESSION['iv'])) {
   //postavljanje parametara potrebnih za dekripciju
    $decryption_key = md5('jed4n j4k0 v3l1k1 kljuc');

    $cipher = "AES-128-CTR";
    $options = 0;

    $decryption_iv = $_SESSION['iv'];

    //prolazak kroz kriptirane datoteke, čitanje njenog sadržaja, dekriptiranje sadržaja i generiranje linkova
    foreach ($files as $file) {
        
        $encryptedFile = fread(fopen($targetDir . $file, "r"), filesize($targetDir . $file));
        $data = openssl_decrypt(
            $encryptedFile,
            $cipher,
            $decryption_key,
            $options,
            $decryption_iv
        );
        
        $_SESSION[$file] = $data;

        echo "<a href=\"download.php?file=" . $file . "\">Preuzmi " . $file . "</a><br>";
    }
} else {
    echo '<p>Nema podataka.</p>';
}
