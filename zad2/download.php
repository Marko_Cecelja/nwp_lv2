<?php

$targetDir = "uploads/";

//pokretanje sessiona, preuzimanje datoteke
session_start();

$data = $_SESSION[$_GET['file']];

header("Cache-Control: no-cache private");
header("Content-Description: File Transfer");
header('Content-disposition: attachment; filename=' . $_GET['file']);
header("Content-Transfer-Encoding: binary");
header('Content-Length: ' . strlen($data));

echo $data;
