<?php

//pokretanje session-a, definiranje parametara potrebnih za enkripciju
session_start();

$encryption_key = md5('jed4n j4k0 v3l1k1 kljuc');
$cipher = 'AES-128-CTR';
$iv_length = openssl_cipher_iv_length($cipher);
$options = 0;

$encryption_iv = random_bytes($iv_length);

$_SESSION['iv'] = $encryption_iv;

//definiranje direktorija i dohvat file-a koji je pristigao kroz formu
$targetDir = "uploads/";
$fileName = basename($_FILES["file"]["name"]);
$targetFilePath = $targetDir . $fileName;
$fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);

if (isset($_POST["submit"]) && !empty($_FILES["file"]["name"])) {
    //postavljanje dozvoljenih tipova podataka i provjera odgovara li file jednom od tih tipova
    $allowTypes = array('pdf', 'jpeg', 'png');
    if (in_array($fileType, $allowTypes)) {

        //čitanje datoteke
        $file_binary = fread(fopen($_FILES["file"]["tmp_name"], "r"), filesize($_FILES["file"]["tmp_name"]));

        //kriptiranje sadržaja datotele
        $data = openssl_encrypt(
            $file_binary,
            $cipher,
            $encryption_key,
            $options,
            $encryption_iv
        );

        //spremanje kriptirane datoteke na server 
        $fd = fopen($targetDir . $fileName, "w");
        fwrite($fd, $data);
        fclose($fd);

        $statusMsg = "Datoteka uspješno dodana";
    } else {
        $statusMsg = 'Neispravni tip datoteke';
    }
} else {
    $statusMsg = 'Odaberite datoteku.';
}
echo $statusMsg . "\n";

echo "<form action=\"zad2_decrypt.php\" method=\"post\"><input type=\"submit\" name=\"submit\" value=\"Dekriptiraj\"></form>";
