<?php
//postavljanje imena baze i direktorija
$db_name = 'testdb';
$dir = "backup/$db_name";

//provjera postoji li direktori i stvaranje novog ukoliko ne postoji
if (!is_dir($dir)) {
    if (!@mkdir($dir)) {
        die("<p>Ne možemo stvoriti direktorij $dir.</p></body></html>");
    }
}

//dohvat trenutnog vremena
$time = time();

//spajanje na bazu podataka
$dbc = @mysqli_connect('localhost', 'root', '', $db_name) or die("<p>Ne možemo se spojiti na bazu $db_name.</p></body></html>");

//dohvat svih tablica
$r = mysqli_query($dbc, 'SHOW TABLES');

//ukoliko postoji barem jedna tablica, radi se backup
if (mysqli_num_rows($r) > 0) {

    echo "<p>Backup za bazu podataka '$db_name'.</p>";

    //dohvaćanje pojedinih tablica
    while (list($table) = mysqli_fetch_array($r, MYSQLI_NUM)) {

        //dohvat podataka iz tablice
        $q = "SELECT * FROM $table";
        $r2 = mysqli_query($dbc, $q);
        $columns = $r2->fetch_fields();

        //ukoliko postoje podaci, stvori tekstualnu datoteku i u nju zapiši INSERT INTO komandu za podatke
        if (mysqli_num_rows($r2) > 0) {

            if ($fp = fopen("$dir/{$table}_{$time}.txt", 'w9')) {

                while ($row = mysqli_fetch_array($r2, MYSQLI_NUM)) {

                    fwrite($fp, "INSERT INTO $db_name (");

                    foreach ($columns as $column) {
                        fwrite($fp, "$column->name");

                        if ($column != end($columns)) {
                            fwrite($fp, ", ");
                        }
                    }

                    fwrite($fp, ")\r\nVALUES (");

                    foreach ($row as $value) {

                        $value = addslashes($value);
                        fwrite($fp, "'$value'");

                        if ($value != end($row)) {
                            fwrite($fp, ", ");
                        } else {
                            fwrite($fp, ")\";");
                        }
                    }

                    fwrite($fp, "\r\n");
                }

                fclose($fp);
                echo "<p>Tablica '$table' je pohranjena.</p>";

                //sažimanje datoteke
                if ($fp2 = gzopen("$dir/{$table}_{$time}.sql.gz", 'w9')) {
                    gzwrite($fp2, file_get_contents("$dir/{$table}_{$time}.txt"));
                    gzclose($fp2);
                } else {
                    echo "<p>Datoteka $dir/{$table}_{$time}.sql.gz se ne može otvoriti.</p>";
                    break;
                }
            } else {
                echo "<p>Datoteka $dir/{$table}_{$time}.txt se ne može otvoriti</p>";
                break;
            }
        }
    }
} else {
    echo "<p>Baza $db_name ne sadrži tablice.</p>";
}
